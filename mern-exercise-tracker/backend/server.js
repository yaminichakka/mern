var express = require('express');
var cors = require('cors');
var mongoose = require('mongoose');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri,{useNewUrlParser:true,useCreateIndex:true});

const connection = mongoose.connection;
connection.once('open',()=>{
    console.log("MongoDB database connection successful");
})

const exercisesRouter = require('./routes/exercises');
const usersRouter = require('./routes/users');


app.use('/exercises',exercisesRouter);
app.use('/users',usersRouter);

app.listen(port,()=>{
    console.log(`Server running at the port ${port}`);
})
