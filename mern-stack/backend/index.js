const express = require('express');
const morgan = require('morgan');
const path = require('path');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI ||'mongodb://localhost:27017/mern',{ useNewUrlParser: true , useUnifiedTopology: true});
//console.log('Mongoose is connected!!!');
mongoose.connection.on('connected',()=>{
    console.log('Mongoose is connected!!!');
});


const cors = require('cors');
const app = express()
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

app.use('/api',require('./routes/index'))
const port = process.env.PORT | 5000

if(process.env.NODE_ENV === 'production')
{
    app.use(express.static('frontend/build'));
    app.get("*",(req,res)=>{
        res.sendFile(path.resolve(__dirname,"../frontend","build","index.html"));
    })
}
app.listen(port,console.log(`Server started at ${port}`))