const express = require('express');
const stack = require('../models/index')
const router = express.Router()

router.get('/',(req,res)=>{
stack.find({},(err,data)=>{
    res.json(data)
})
})

router.get('/:id',(req,res)=>{
    stack.findById(req.params.id,(err,data)=>{
        res.json(data)
    })
})
    router.delete('/:id',async (req,res)=>{
        await stack.findByIdAndDelete(req.params.id)
        res.json({'message':'deleted'})
    })
    router.post('/',(req,res)=>{
        user =new stack({
            name:req.body.name,
            email:req.body.email,
            password:req.body.password
        })
        user.save((res)=>{
            res.json(user);
        })
    })
    router.put('/:id',async (req,res)=>{
        await stack.findByIdAndUpdate(req.params.id,req.body)
        res.json({'message':'updated'})
    })
module.exports = router