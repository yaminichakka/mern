import React from 'react';
import  './App.css'

class App extends React.Component {
  constructor(props) {
    super(props)
    
    this.state = { users:[], apiResponse:"" }
  }
  callAPI(){
    fetch("http://localhost:9000/test")
     .then(res=>res.text())
     .then(res=>this.setState({apiResponse:res}));
  }
  componentWillMount(){
    this.callAPI();
  }
  componentDidMount(){
    fetch("http://localhost:9000/users")
    .then(response => response.json())
    .then(res => {
      if(res && res.data) {
        this.setState({users:[...this.state.users,...res.data]})
      }
    });
  }
  
  renderUsers() {
      if(this.state.users.length <= 0){
        return <div>Loading...</div>
      }
      else{
      return this.state.users.map((val,key) => {
      return <div key={key}>{val.name} | {val.age}</div>
      });
      }
  }
  render(){
  return (
    <div className="App">
        <p>
          {this.renderUsers()}
          {this.state.apiResponse}
        </p>
    </div>
  );
}
}
export default App;
